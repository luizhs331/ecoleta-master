import express from 'express'; // vem do node modules 
import routes  from './routes' 
import path from 'path';
import cors from 'cors'

const app = express();

// utilizado para o express entender requisicoes json
app.use(cors());
app.use( express.json() );
app.use( routes );

app.use('/uploads', express.static( path.resolve( __dirname, '..','uploads' ) ) );
// pegando imagens por metodo statico express

app.listen(3333);


// request param exemplo ( /users/:id)
// query parametros quem vem na propria rota normalmente opcionais normalmente para filtros
// request body : parametros para criacao e atualizacao de informacoes 

// const users = [
//     'calors',
//     'Meee',
//     'Maicon',
//     'Micklos2'
// ]

// app.get( '/users' , ( request, response ) => {
//     console.log('Listagem de Usuarios');
//     const search = String( request.query.search )
//     console.log( search );
//     const filterUsers = search ? users.filter( user => user.includes( search ) ) : users
//     response.json( filterUsers );
// });

// app.get( '/users/:id' , ( request , response ) =>{
//        const id = Number( request.params.id );
//        return response.json( users[id] )
// })

// app.post( '/users' , ( request , response ) =>{
//     const data  = request.body
//     console.log( data );

//     const user = {
//         nome : data.nome,
//         email : data.email
//     }

//     return response.json( user );

// })