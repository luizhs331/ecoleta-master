import React from 'react';
import ReactDOM from 'react-dom';
 // esta falando para o react se integrar com a  web
import App from './App';

// dom arvore de elementos do html

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
