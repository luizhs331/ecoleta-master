import { Route, BrowserRouter } from  'react-router-dom';
import React from 'react';

import Home from './pages/Home'
import CreatePoints from './pages/CreatePoint'

const Routes = () => {
    return(
        <BrowserRouter>                    
            <Route component={Home} path="/" exact />
             <Route component={CreatePoints} path="/create-point"/>
        </BrowserRouter>
    );
}

 // existe um problema que ele nao verifica a rota completa sem o exact 

export default Routes;